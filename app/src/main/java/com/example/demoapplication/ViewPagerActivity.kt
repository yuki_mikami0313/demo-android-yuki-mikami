package com.example.demoapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.viewpager.*

class ViewPagerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.viewpager)

        supportActionBar?.title = "View Pager"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        /// フラグメントのリストを作成
        val fragmentList = arrayListOf<Fragment>(
            sample1(),
            sample2_frag(),
            sample3_frag()
        )

        /// adapterのインスタンス生成
        val adapter = SamplePagerAdapter(supportFragmentManager, fragmentList)
        /// adapterをセット
        viewPager.adapter = adapter
    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }


}