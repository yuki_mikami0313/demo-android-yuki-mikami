package com.example.demoapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

//class MainActivity : AppCompatActivity()
class MainActivity : AppCompatActivity() {


    /*private var list: ArrayList<String> = ArrayList()
    fun add(element: String) {"a"}
    fun add(element: E): Boolean
    */
    var mutableList: MutableList<String> = mutableListOf("Input Text","Slider","Drag And Drop","Map View","Play Video","Web View","View Pager","Grid View","Unquete","Scroll")



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.title = "Demo App"

        /// 表示するテキスト配列を作る [テキスト0, テキスト1, ....]
        //val list = Array<String>(10) {"テキスト$it"}
        val adapter = CustomAdapter(mutableList)
        val layoutManager = LinearLayoutManager(this)

        // アダプターとレイアウトマネージャーをセット
        simpleRecyclerView.layoutManager = layoutManager
        simpleRecyclerView.adapter = adapter
        simpleRecyclerView.setHasFixedSize(true)

        // インターフェースの実装
        adapter.setOnItemClickListener(object:CustomAdapter.OnItemClickListener{
            override fun onItemClickListener(view: View, position: Int, clickedText: String) =
                if(clickedText == "Slider") {
                    val intent = Intent(applicationContext, Slider::class.java)
                    startActivity(intent)

                }else if(clickedText == "Map View") {
                    val intent = Intent(applicationContext, MapsActivity::class.java)
                    startActivity(intent)

                }else if(clickedText == "Web View") {
                    val intent = Intent(applicationContext, WebActivity::class.java)
                    startActivity(intent)

                }else if(clickedText == "View Pager") {
                    val intent = Intent(applicationContext, ViewPagerActivity::class.java)
                    startActivity(intent)

                }else if(clickedText == "Play Video") {
                    val intent = Intent(applicationContext, PlayVideoActivity::class.java)
                    startActivity(intent)


                } else if(clickedText == "Input Text") {
                    val intent = Intent(applicationContext, FormActivity::class.java)
                    startActivity(intent)

                }else{
                    print("a")
                }
        })
    }
}
