package com.example.demoapplication

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.form2.*

class FormActivity2 : AppCompatActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.form2)

        supportActionBar?.title = "アンケート"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        intentTextView.text = intent.getStringExtra(FormActivity.EXTRA_TEXTDATA)
       // form2nenrei.text = intent.getStringExtra(FormActivity.nenreii)
        form2nenn.text = intent.getStringExtra(FormActivity.nannen)
        form2tuki.text = intent.getStringExtra(FormActivity.nangatu)
        form2niti.text = intent.getStringExtra(FormActivity.nanniti)
        form2nenrei.text = intent.getIntExtra(FormActivity.nenreii, 0).toString()

        val genderIndex = intent.getStringExtra("gender")
        val hakocheckbox = intent.getStringExtra("checkbox")
        val hakocheckbox2 = intent.getStringExtra("checkbox2")
        val hakocheckbox3 = intent.getStringExtra("checkbox3")
        val hakocheckbox4 = intent.getStringExtra("checkbox4")
        val hakocheckbox5 = intent.getStringExtra("checkbox5")



        val tvSubGenderIndex = findViewById<TextView>(R.id.tv_subgenderIndex)
        tvSubGenderIndex.text = genderIndex

        val Subcheckbox = findViewById<TextView>(R.id.sub_checkbox)
        Subcheckbox.text = hakocheckbox

        val Subcheckbox2 = findViewById<TextView>(R.id.sub_checkbox2)
        Subcheckbox2.text = hakocheckbox2

        val Subcheckbox3 = findViewById<TextView>(R.id.sub_checkbox3)
        Subcheckbox3.text = hakocheckbox3

        val Subcheckbox4 = findViewById<TextView>(R.id.sub_checkbox4)
        Subcheckbox4.text = hakocheckbox4

        val Subcheckbox5 = findViewById<TextView>(R.id.sub_checkbox5)
        Subcheckbox5.text = hakocheckbox5



        topbutton.setOnClickListener{
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        }

    }










    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

}