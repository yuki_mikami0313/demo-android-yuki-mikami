package com.example.demoapplication


import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity


@SuppressLint("Registered")
class WebActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.web_view)

        supportActionBar?.title = "Web View"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


            val myWebView = findViewById<WebView>(R.id.web_view)
            myWebView.settings.javaScriptEnabled = true
            myWebView.loadUrl("https://www.sonix.asia")



    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}