package com.example.demoapplication


import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.widget.MediaController
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.playvideo.*
import kotlinx.coroutines.delay


class PlayVideoActivity : AppCompatActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.playvideo)

        supportActionBar?.title = "PlayVideo"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        supportActionBar?.hide()
        supportActionBar?.show()

        imageButton.setOnClickListener{
        Handler(mainLooper).postDelayed({
            var moviePath = Uri.parse("android.resource://" + packageName + "/" + R.raw.taki)
            video.setVideoURI(moviePath)

            //seyVideoXXXメゾットは非同期なので、リスナーを設定し、準備が完了してから再生するように実装する
            video.setOnPreparedListener {
                video.start()

                //再生メニューの表示
                video.setMediaController(MediaController(this))

            }


        },200)
        }



    }

    // 画面が回転した時に呼ばれる
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        // 横向きかどうかを判定する
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            supportActionBar?.hide()
        } else {
            supportActionBar?.show()
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}