package com.example.demoapplication


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*

import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.form.*
import kotlinx.android.synthetic.main.form.form2nen
import kotlinx.android.synthetic.main.form.view.*
import android.widget.RadioGroup
import java.util.*


class FormActivity : AppCompatActivity() {

    private var yearlist = listOf("2000", "2001", "2002", "2003","2004","2005","2006","2007","2008","2009","2010","2011")
    private var monthlist = listOf("1", "2", "3", "4","5","6","7","8","9","10","11","12")
    private var daylist = listOf("1", "2", "3", "4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31")
    companion object {
        const val EXTRA_TEXTDATA = "com.usaco_pg.intentsample.TEXTDATA"
        const val nenreii = "com.usaco_pg.nenreii.TEXTDATA"
        const val nannen = "com.usaco_pg.nannen.TEXTDATA"
        const val nangatu = "com.usaco_pg.nangatu.TEXTDATA"
        const val nanniti = "com.usaco_pg.nanniti.TEXTDATA"
       // const val gender = "com.usaco_pg.gender.TEXTDATA"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.form)

        supportActionBar?.title = "アンケート"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

       val radioGroup = findViewById<RadioGroup>(R.id.radiogroup)
        radioGroup.check(R.id.radio_1)



// 選択状態のRadioButtonのIDを取得
      //  val id = radioGroup.checkedRadioButtonId




        //ラジオボタンの処理

        var rg_gender: RadioGroup
        var radio_1: RadioButton
        var radio_2: RadioButton
        var radio_3: RadioButton
        var index = ""



        rg_gender = findViewById(R.id.radiogroup)
        radio_1 = findViewById(R.id.radio_1)
        radio_2 = findViewById(R.id.radio_2)
        radio_3 = findViewById(R.id.radio_3)


        //チェックボックスの処理

        var hako = ""
        var hako2 = ""
        var hako3 = ""
        var hako4 = ""
        var hako5 = ""

        val checkBox = findViewById<CheckBox>(R.id.checkBox)
        val checkBox2 = findViewById<CheckBox>(R.id.checkBox2)
        val checkBox3 = findViewById<CheckBox>(R.id.checkBox3)
        val checkBox4 = findViewById<CheckBox>(R.id.checkBox4)
        val checkBox5 = findViewById<CheckBox>(R.id.checkBox5)




        if(rg_gender.checkedRadioButtonId != -1) {
            if (radio_1.isChecked)
                index = "男性"
            else if (radio_2.isChecked)
                index = "女性"
            else if (radio_3.isChecked)
                index = "その他"
        }


        val spyadpt= ArrayAdapter(applicationContext,android.R.layout.simple_spinner_item,yearlist)
        spyadpt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spy.adapter=spyadpt

        val spmadpt= ArrayAdapter(applicationContext,android.R.layout.simple_spinner_item,monthlist)
        spmadpt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spm.adapter=spmadpt

        val spdadpt= ArrayAdapter(applicationContext,android.R.layout.simple_spinner_item,daylist)
        spdadpt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spd.adapter=spdadpt

        spy.onItemSelectedListener=object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val spp=p0 as Spinner
                // t0.text=spp.selectedItem.toString()
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        }

        spm.onItemSelectedListener=object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val spp=p0 as Spinner
                // t0.text=spp.selectedItem.toString()
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        }

        spd.onItemSelectedListener=object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val spp=p0 as Spinner
                // t0.text=spp.selectedItem.toString()
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        }

            send.setOnClickListener{
                val intent = Intent(this, FormActivity2::class.java)

                if(rg_gender.checkedRadioButtonId != -1) {
                    if (radio_1.isChecked)
                        index = "男性"
                    else if (radio_2.isChecked)
                        index = "女性"
                    else if (radio_3.isChecked)
                        index = "その他"
                }

                if (checkBox.isChecked)
                    hako = "インターネット"
                else
                    hako = ""

                if (checkBox2.isChecked)
                    hako2  = "雑誌記事"
                else
                    hako2 = ""

                if (checkBox3.isChecked)
                    hako3 = "友人、知人など"
                else
                    hako3 = ""

                if (checkBox4.isChecked)
                    hako4 = "セミナー"
                else
                    hako4 = ""

                if (checkBox5.isChecked)
                    hako5 = "その他"
                else
                    hako5 = ""




                intent.putExtra(EXTRA_TEXTDATA, name.text.toString())
                intent.putExtra(nannen, spy.selectedItem.toString())
                intent.putExtra(nangatu, spm.selectedItem.toString())
                intent.putExtra(nanniti, spd.selectedItem.toString())
                intent.putExtra("gender", index.toString())
                intent.putExtra(nenreii, editnenrei.text.toString().toInt())
                intent.putExtra("checkbox", hako.toString())
                intent.putExtra("checkbox2", hako2.toString())
                intent.putExtra("checkbox3", hako3.toString())
                intent.putExtra("checkbox4", hako4.toString())
                intent.putExtra("checkbox5", hako5.toString())





                startActivity(intent)



                }
            }








    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}