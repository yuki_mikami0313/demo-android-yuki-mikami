package com.example.demoapplication

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.silder.*

class Slider : AppCompatActivity()  {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.silder)

        supportActionBar?.title = "Slider Sample"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        fun onSupportNavigateUp(): Boolean {
            finish()
            return super.onSupportNavigateUp()
        }


        val sb = listOf(sbR,sbG,sbB)

        sb.forEach(){
            it.setOnSeekBarChangeListener(object:SeekBar.OnSeekBarChangeListener{
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    sbChanged()
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {

                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {

                }
            })
        }
        sbChanged()

    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()

    }
    fun sbChanged(){
        et0.setText(String.format("#%02X%02X%02X",sbR.progress,sbG.progress,sbB.progress))
        LL0Redraw()
    }
    fun LL0Redraw(){
        view.setBackgroundColor(Color.rgb(sbR.progress,sbG.progress,sbB.progress))
    }
}